package com.twuc.webApp.web;

import org.springframework.hateoas.*;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

//class UserResource extends ResourceSupport {
//    private final User user;
//
//    UserResource(final User user) {
//        final long id = user.getId();
//
//        this.user = user;
//
//        add(linkTo(methodOn(UserController.class).getUser(id)).withSelfRel());
//        add(linkTo(methodOn(UserController.class).updateUser(id, user)).withRel("edit"));
//        add(linkTo(methodOn(UserController.class).getUserProperty(id, null)).withRel("getProperty"));
//    }
//
//    public User getUser() {
//        return user;
//    }
//}

class UserResource extends Resource {
    public UserResource(User user, Link... links) {
        super(user, links);
    }
}

